<?php

if(!isset($url))
{
	exit();
}

class Site
{
	public function redirect($to)
	{
		header("Location: ".$url.$to);
		exit();
	}
}

?>