<?php

if(!isset($_pdo))
{
	exit();
}

class Session
{
	private $pdo = null;

	public function __construct($_pdo)
	{
		$this->pdo = $_pdo;
	}

	public function validate($logged)
	{
		if(isset($_SESSION["login"]) && isset($_SESSION["senha"]))
		{
			$login = preg_replace("/\"|\n|\r/", "", $_SESSION["login"]);
			$senha = preg_replace("/\"|\n|\r/", "", $_SESSION["senha"]);

			if($login == "admin" && $senha == md5("admin")) // Admin geral
			{
				$_SESSION["type"] = 1;
				return;
			}
			else
			{
				$sql = $this->pdo->prepare("SELECT * FROM capitulo WHERE login=:login AND senha=:senha");
				$sql->bindParam(":login", $login, PDO::PARAM_STR);
				$sql->bindParam(":senha", $senha, PDO::PARAM_STR);
				$sql->execute();

				if($sql->rowCount() > 0) // Capítulo
				{
					$row = $sql->fetchObject();

					$_SESSION["user"] = $row;
					$_SESSION["type"] = 2;

					return;
				}
				else
				{
					$sql = $this->pdo->prepare("SELECT * FROM capitulo WHERE login=:login AND senha=:senha");
					$sql->bindParam(":login", $login, PDO::PARAM_STR);
					$sql->bindParam(":senha", $senha, PDO::PARAM_STR);
					$sql->execute();

					if($sql->rowCount() > 0) // Capítulo
					{
						$row = $sql->fetchObject();

						$_SESSION["user"] = $row;
						$_SESSION["type"] = 3;

						return;
					}
					else // Login inválido
					{
						session_destroy();
					}
				}
			}
		}
		else
		{
		}

		if(!$logged)
		{
			Site::redirect("Login");
		}
	}

	public function start($login, $senha)
	{
		if(isset($_SESSION))
		{
			$_SESSION["login"] = $login;
			$_SESSION["senha"] = md5($senha);

			Site::redirect("Inicial");
		}
	}
}

?>