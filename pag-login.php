<?php

if(!isset($pag))
{
	exit();
}

if(isset($_POST["login"]) && isset($_POST["senha"]))
{
	$login = preg_replace("/\n|\r|\"/", "", $_POST["login"]);
	$senha = preg_replace("/\n|\r|\"/", "", $_POST["senha"]);

	$_session->start($login, $senha);
}

?><!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Stellium</title>
	<!-- Fonte -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
	<!-- Stellium -->
	<link rel="stylesheet" href="<?php echo $url."css/style.min.css"; ?>">
</head>
<body>
	<div class="page-bg"></div>

	<div class="page">
		<div class="login-box">
			<div class="login-logo">
				<img src="<?php echo $url."img/logo.png"; ?>">
			</div>

			<div class="login-container">
				<form id="login-form">
					<div class="login-field">
						<div class="login-label">Servidor:</div>
						<select name="login" id="login-servidor" class="login-control"><?php

						$sql = $_pdo->prepare("SELECT id_servidor, nome FROM servidor ORDER BY id ASC");
						$sql->execute();

						if($sql->rowCount() > 0)
						{
							while($row = $sql->fetchObject())
							{
								echo "<option value=\"".$row->id_servidor."\">".$row->nome."</option>";
							}
						}
						else
						{
							echo "<option value=\"\">Nenhum servidor encontrado</option>";
						}

						?></select>
					</div>

					<div class="login-field">
						<div class="login-label">Nome de usuário:</div>
						<input type="text" name="login" id="login-login" class="login-control">
					</div>

					<div class="login-field">
						<div class="login-label">Senha:</div>
						<input type="password" name="senha" id="login-senha" class="login-control">
					</div>

					<div class="login-btns">
						<div class="login-btns-cell">
							<a href="<?php echo $url; ?>" class="login-lembrar-senha">Lembrar senha</a>
						</div>

						<div class="login-btns-cell login-btns-cell-btn">
							<button class="login-btn login-btn-entrar">Entrar</button>
						</div>

						<div class="login-btns-cell login-btns-cell-btn">
							<button class="login-btn login-btn-cadastrar">Cadastrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<!-- GSAP -->
<script type="text/javascript" src="<?php echo $url."js/TweenMax.min.js"; ?>"></script>
<!-- jQuery -->
<script type="text/javascript" src="<?php echo $url."js/jQueryOFC.min.js"; ?>"></script>
<!-- Stellium -->
<script type="text/javascript" src="<?php echo $url."js/jQuery.min.js"; ?>"></script>
</html>