-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 21-Ago-2015 às 05:59
-- Versão do servidor: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stellium`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `conta`
--

CREATE TABLE IF NOT EXISTS `conta` (
  `id` bigint(20) unsigned NOT NULL,
  `id_servidor` bigint(20) unsigned NOT NULL,
  `id_conta` text NOT NULL,
  `login` text NOT NULL,
  `senha` text NOT NULL,
  `email` text NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` bigint(20) unsigned NOT NULL,
  `id_item` text NOT NULL,
  `nome` text NOT NULL,
  `imagem` text NOT NULL,
  `atributos` text NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `monstro`
--

CREATE TABLE IF NOT EXISTS `monstro` (
  `id` bigint(20) unsigned NOT NULL,
  `id_monstro` text NOT NULL,
  `nome` text NOT NULL,
  `level` bigint(20) unsigned NOT NULL,
  `experiencia` bigint(20) unsigned NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `personagem`
--

CREATE TABLE IF NOT EXISTS `personagem` (
  `id` bigint(20) unsigned NOT NULL,
  `id_conta` bigint(20) unsigned NOT NULL,
  `id_personagem` text NOT NULL,
  `nome` text NOT NULL,
  `level` bigint(20) unsigned NOT NULL,
  `experiencia` bigint(20) unsigned NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `personagem_batalha`
--

CREATE TABLE IF NOT EXISTS `personagem_batalha` (
  `id` bigint(20) unsigned NOT NULL,
  `id_personagem` bigint(20) unsigned NOT NULL,
  `id_monstro` bigint(20) unsigned NOT NULL,
  `informacao` text NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `personagem_item`
--

CREATE TABLE IF NOT EXISTS `personagem_item` (
  `id` bigint(20) unsigned NOT NULL,
  `id_personagem` bigint(20) unsigned NOT NULL,
  `id_item` bigint(20) unsigned NOT NULL,
  `nome` text NOT NULL,
  `atributos` text NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `servidor`
--

CREATE TABLE IF NOT EXISTS `servidor` (
  `id` bigint(20) unsigned NOT NULL,
  `id_servidor` text NOT NULL,
  `exp_rate` float unsigned NOT NULL,
  `gold_rate` float unsigned NOT NULL,
  `modificado` datetime NOT NULL,
  `criado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conta`
--
ALTER TABLE `conta`
  ADD PRIMARY KEY (`id`,`id_servidor`),
  ADD KEY `id_servidor` (`id_servidor`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monstro`
--
ALTER TABLE `monstro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personagem`
--
ALTER TABLE `personagem`
  ADD PRIMARY KEY (`id`,`id_conta`),
  ADD KEY `id_conta` (`id_conta`);

--
-- Indexes for table `personagem_batalha`
--
ALTER TABLE `personagem_batalha`
  ADD PRIMARY KEY (`id`,`id_personagem`,`id_monstro`),
  ADD KEY `id_personagem` (`id_personagem`),
  ADD KEY `id_monstro` (`id_monstro`);

--
-- Indexes for table `personagem_item`
--
ALTER TABLE `personagem_item`
  ADD PRIMARY KEY (`id`,`id_personagem`,`id_item`),
  ADD KEY `id_personagem` (`id_personagem`),
  ADD KEY `id_item` (`id_item`);

--
-- Indexes for table `servidor`
--
ALTER TABLE `servidor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conta`
--
ALTER TABLE `conta`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `monstro`
--
ALTER TABLE `monstro`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personagem`
--
ALTER TABLE `personagem`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personagem_batalha`
--
ALTER TABLE `personagem_batalha`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personagem_item`
--
ALTER TABLE `personagem_item`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `servidor`
--
ALTER TABLE `servidor`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `conta`
--
ALTER TABLE `conta`
  ADD CONSTRAINT `conta_ibfk_1` FOREIGN KEY (`id_servidor`) REFERENCES `servidor` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `personagem`
--
ALTER TABLE `personagem`
  ADD CONSTRAINT `personagem_ibfk_1` FOREIGN KEY (`id_conta`) REFERENCES `conta` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `personagem_batalha`
--
ALTER TABLE `personagem_batalha`
  ADD CONSTRAINT `personagem_batalha_ibfk_1` FOREIGN KEY (`id_personagem`) REFERENCES `personagem` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `personagem_batalha_ibfk_2` FOREIGN KEY (`id_monstro`) REFERENCES `monstro` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `personagem_item`
--
ALTER TABLE `personagem_item`
  ADD CONSTRAINT `personagem_item_ibfk_1` FOREIGN KEY (`id_personagem`) REFERENCES `personagem` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `personagem_item_ibfk_2` FOREIGN KEY (`id_item`) REFERENCES `item` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
