<?php

require_once(dirname(__FILE__)."/conn/config.php");

$pag = isset($_GET["pag"]) ? explode("/", $_GET["pag"]) : array();

if(is_array($pag))
{
	if(isset($pag[0]))
	{
		$login = ($pag[0] == "Login");

		$_session->validate($login);

		$page = "pag-".$pag[0].".php";

		if(file_exists($page))
		{
			if($login)
			{
				require_once(dirname(__FILE__)."/".$page);
			}
			else
			{
				require_once(dirname(__FILE__)."/".$page);
			}
		}
		else
		{
		}
	}
	else
	{
		$_site->redirect("Inicial");
	}
}

exit();

?>