<?php

mb_internal_encoding("UTF-8");
date_default_timezone_set("America/Sao_Paulo");

try
{
    $_pdo = new PDO("mysql:host=localhost;dbname=manager", "root", "nfr112233");
}
catch(PDOException $e)
{
    print "Error!: ".$e->getMessage()."<br/>";
    exit();
}

session_start();
session_destroy();

$url = "http://localhost/";

// Classes
require_once(dirname(__FILE__)."/../class/session.php");
require_once(dirname(__FILE__)."/../class/site.php");

$_session = new Session($_pdo);
$_site = new Site();

?>