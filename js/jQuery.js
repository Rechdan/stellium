$(function(){

	// Geral

	var url = "http://" + window.location.host + "/";

	// Escolher um servidor

	$(document).on("submit", "#login-form", function(event){
		event.preventDefault();

		var This = $(this);
		var Data = This.serialize();

		console.log(Data);

		if(Data)
		{
			sendAjax({
				url: url + "ajax/login.php",
				data: Data,
				beforeSend: function(){
					TweenMax.fromTo(".login-container", 0.3, {
						autoAlpha: 1
					}, {
						autoAlpha: 0
					});
				},
				afterSend: function(ans){
					TweenMax.fromTo(".login-container", 0.3, {
						autoAlpha: 0
					}, {
						autoAlpha: 1
					});
				}
			});
		}
	});

	// Função ajax

	function sendAjax(options){
		var options = $.extend({
			url: "",
			data: {},
			beforeSend: null,
			afterSend: null
		}, options);

		if(options.beforeSend)
		{
			options.beforeSend();
		}

		$.ajax({
			url: options.url,
			type: "POST",
			data: options.data,
			success: function(ans){
				if(options.afterSend)
				{
					options.afterSend(ans);
				}
			}
		});
	}

});