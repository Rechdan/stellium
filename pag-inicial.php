<?php

if(!isset($pag))
{
	exit();
}

?><!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Capítulo 371 Taubaté</title>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo $url."css/bootstrap.min.css"; ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo $url."css/font-awesome.min.css"; ?>">
	<!-- Admin -->
	<link rel="stylesheet" href="<?php echo $url."css/AdminLTE.min.css"; ?>">
	<!-- Theme -->
	<link rel="stylesheet" href="<?php echo $url."css/skin-blue.min.css"; ?>">
</head>
<body class="skin-blue">
	<div class="wrapper">
		<div class="main-header">
			<a href="<?php echo $url."Inicial"; ?>" class="logo">
				<span class="logo-mini"><b>C</b>371</span>
				<span class="logo-lg">Capítulo<b>371</b></span>
			</a>

			<div class="navbar navbar-static-top" role="navigation">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"></a>
			</div>
		</div>

		<div class="main-sidebar">
			<div class="sidebar">
				<ul class="sidebar-menu">
					<li class="header">MENU</li>

					<li class="active">
						<a href="">
							<span><span class="fa fa-home fa-fw"></span> Inicial</span>
						</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="content-wrapper">
			<div class="content-header">
				<h1>
					<span>Administração</span>
				</h1>
			</div>

			<div class="content">
				<div class="row">
					<div class="col-md-12"></div>
				</div>
			</div>
		</div>

		<div class="main-footer">
			<div class="text-center">Feito por <b>Nelson F. Rechdan</b> para o <b>Capítulo de Taubaté, nº 371</b>. <?php echo date("Y"); ?>.</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?php echo $url."js/jQuery-2.1.4.min.js"; ?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo $url."js/bootstrap.min.js"; ?>"></script>
	<!-- Admin -->
	<script src="<?php echo $url."js/admin.min.js"; ?>"></script>
</body>
</html>